pragma solidity ^0.4.11;
import "zeppelin-solidity/contracts/token/StandardToken.sol";

contract ExampleToken is StandardToken {
string public name = "XIO CHANGE"; 
string public symbol = "XIO";
uint public decimals = 0;
uint public INITIAL_SUPPLY = 10000 * (10 ** decimals);

function ExampleToken() {
totalSupply = INITIAL_SUPPLY;
balances[msg.sender] = INITIAL_SUPPLY;
}
}